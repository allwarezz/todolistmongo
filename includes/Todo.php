<?php
namespace DataHandling;

class Todo extends FormHandle
{

    protected static function sanitize($fields)
    {
        $errors = array();
        $fields['testo'] = self::cleanInput($fields['testo']);
        return $fields;
    }

    public static function insertData($form_data)
    {

        $fields = array(
            'testo' => $form_data['testo'],
        );
        if ($fields['testo'] === '') {
            header('Location: ' . MY_BASE_URL . 'index.php?stato=ko'
                . '&messages=Non è possibile inserire un todo vuoto');
            exit;
        }
        $fields = self::sanitize($fields);
        $db_conn = \DBHandle\getConnection();

        /*$query = $mysqli->prepare('INSERT INTO todos (testo) VALUES (?)');
        $query->bind_param('s', $fields['testo']);
        $query->execute();

        if ($query->affected_rows === 0) {
        error_log('Errore MySQL: ' . $query->error_list[0]['error'] . "\n", 3, 'my-errors.log');
        header('Location: '.MY_BASE_URL.'index.php?stato=ko'
        . '&messages=Non è stato possibile inserire il todo, riprova più tardi');
        exit;
        }
        $query->close();*/
        $today = new \DateTime(date('Y-m-d'), new \DateTimeZone('UTC'));
        $resInsert = $db_conn->tasks->insertOne([
            'task' => $fields['testo'],
            'completato' => false,
            'creazione' => new \MongoDB\BSON\UTCDateTime(($today)->getTimestamp() * 1000),
        ]);
        if ($resInsert->getInsertedCount() > 0) {
            header('Location: ' . MY_BASE_URL . 'index.php?stato=ok&messages=Todo inserito con successo');
            exit;
        }
        header('Location: ' . MY_BASE_URL . 'index.php?stato=ko&messages=Errore Generico inserimento Todo');
        exit;
    }

    public static function selectData($idTodo = null)
    {
        $db_conn = \DBHandle\getConnection();

        if (isset($idTodo)) {

            //DA IMPLEMENTARE
            //$idTodo = intval($idTodo);
            $resFind = $db_conn->tasks->findOne([
                '_id' => new \MongoDB\BSON\ObjectID($idTodo),
            ], ['
            projection' => array(
                'task' => 1,
                'completato' => 0,
                'creazione' => 0,
                'completamento' => 0,
                '_id' => 1,
            )]);

            //$query = $mongoTodo->prepare('SELECT * FROM todos WHERE id = ?');
            /*if (is_bool($query)) {
            throw new Exception('Query non valida. $mongoTodo->prepare ha restituito false.');
            }*/
            // $query->bind_param('i', $idTodo);
            //$query->execute();
            //$query = $query->get_result();

        } else {
            //$query = $mongoTodo->query('SELECT * FROM todos ORDER BY stato, dataCreazione DESC');
            $resFind = $db_conn->tasks->find([], ['sort' => ['completato' => 1, 'creazione' => -1]]);
        }
        if (!$resFind) {
            error_log("Errore MongoDB non è stato possibile recuperare alcun todo" . "\n", 3, 'my-errors.log');
            header('Location: ' . MY_BASE_URL . '?stato=ko'
                . '&messages=Ops, Non è stato possibile recuperare alcun Todo');
            exit;
        }
        $results = array();

        /*while ($row = $query->fetch_assoc()) {
        $row['testo'] = stripslashes($row['testo']);
        $results[] = $row;
        }*/
        if (get_class($resFind) !== 'MongoDB\Model\BSONDocument') {
            foreach ($resFind as $document) {
                $results[] = iterator_to_array($document);
            }
        } else {
            $results[] = iterator_to_array($resFind);
        }

        return $results;
    }

    public static function deleteData($id)
    {
        $db_conn = \DBHandle\getConnection();
        $messages = '';
        if ($id === 'tutti') {
            // $result = $query = $mysqli->query('DELETE FROM todos');
            $taskDeleted = $db_conn->tasks->drop();
            if (get_class($taskDeleted) === 'MongoDB\Model\BSONDocument') {
                header('Location: ' . MY_BASE_URL . '?stato=ok&messages=Tutti i Todo sono stati eliminati');
            } else {
                error_log("Errore MySql Nella cancellazione di tutti i Todo" . "\n", 3, 'my-errors.log');
                header('Location: ' . MY_BASE_URL . '?stato=ko'
                    . '&messages=Ops, Non è stato possibile rimuovere i Todo');
            }
        } else {
            $taskDeleted = $db_conn->tasks->deleteOne([
                "_id" => new \MongoDB\BSON\ObjectID($id),
            ]);

            if ($taskDeleted->getDeletedCount() < 1) {
                error_log("Errore MongoDB nella cancellazione del Todo" . "\n", 3, 'my-errors.log');
                header('Location: ' . MY_BASE_URL . '?stato=ko'
                    . '&messages=Ops, Non è stato possibile rimuovere il Todo');
                exit;
            }

            header('Location: ' . MY_BASE_URL . '?stato=ok&messages=Todo Eliminato Correttamente');
        }
        exit;
    }

    public static function updateData($form_data)
    {
        $db_conn = \DBHandle\getConnection();
        if ($form_data['id'] === 'tutti') {
            $stato_todos = ($form_data['stato'] == 1) ? true : false;
            $today = new \DateTime(date('Y-m-d'), new \DateTimeZone('UTC'));
            $data_completamento = ($stato_todos) ?
            new \MongoDB\BSON\UTCDateTime(($today)->getTimestamp() * 1000)
            :
            null;
            $taskUpdated = $db_conn->tasks->updateMany([], [
                '$set' => ['completato' => $stato_todos, 'completamento' => $data_completamento],
            ]);
            if ($taskUpdated->getModifiedCount() < 1 || $taskUpdated === null) {
                header('Location: ' . MY_BASE_URL . 'index.php?stato=ko'
                    . '&messages=Non è stato possibile aggiornare i Todo');
                exit;
            }
            header('Location: ' . MY_BASE_URL . 'index.php?stato=ok'
                . '&messages=Tutti i Todo sono stati aggiornati');
            exit;
        } else {
            $sql = '';
            $id = $form_data['id'];
            $mysqli = \DBHandle\getConnection();
            if (!isset($form_data['stato']) && isset($form_data['testo'])) {
                $fields = array(
                    'testo' => $form_data['testo'],
                );
                if ($fields['testo'] === '') {
                    header('Location: ' . MY_BASE_URL . 'index.php?stato=ko'
                        . '&messages=Non è possibile inserire un todo vuoto');
                    exit;
                }
                $fields = self::sanitize($fields);
                try {
                    $taskUpdated = $db_conn->tasks->updateMany([
                        '_id' => new \MongoDB\BSON\ObjectID($id),
                    ], [
                        '$set' => ['task' => $fields['testo']],
                    ]);
                    if ($taskUpdated->getModifiedCount() < 1 || $taskUpdated === null) {
                        header('Location: ' . MY_BASE_URL . 'index.php?stato=ko'
                            . '&messages=Non è stato possibile aggiornare il Todo');
                        exit;
                    }
                } catch (Exception $e) {
                    error_log("Errore PHP in linea {$e->getLine()}: " . $e->getMessage() . "\n", 3, 'my-errors.log');
                    header('Location: ' . MY_BASE_URL . '?stato=ko'
                        . '&messages=Ops, Non è stato possibile Aggiornare il Todo');
                    exit;
                }
            } else {
                $stato_todos = ($form_data['stato'] == 1) ? true : false;
                $today = new \DateTime(date('Y-m-d'), new \DateTimeZone('UTC'));
                $data_completamento = ($stato_todos) ?
                new \MongoDB\BSON\UTCDateTime(($today)->getTimestamp() * 1000)
                :
                null;
                $taskUpdated = $db_conn->tasks->updateMany([
                    '_id' => new \MongoDB\BSON\ObjectID($id),
                ], [
                    '$set' => ['completato' => $stato_todos, 'completamento' => $data_completamento],
                ]);
                if ($taskUpdated->getModifiedCount() < 1 || $taskUpdated === null) {
                    header('Location: ' . MY_BASE_URL . 'index.php?stato=ko'
                        . '&messages=Non è stato possibile aggiornare il Todo');
                    exit;
                }
            }
            //$query->execute();
            header('Location: ' . MY_BASE_URL . 'index.php?stato=ok&messages=Il Todo è stato aggiornato');
            exit;
        }
    }
}
