<?php
namespace DataHandling\Utils;

function get_table_head($assoc_array)
{
    $keys = array_keys($assoc_array);
    $html = '';

    foreach ($keys as $key) {
        if ($key === '_id') {
            $html .= '<th scope="col">' . ucwords('id') . '</th>';
            continue;
        }
        $html .= '<th scope="col">' . ucwords($key) . '</th>';
    }

    if (!in_array('completamento', $keys)) {
        $html .= '<th scope="col">' . ucwords('completamento') . '</th>';
    }
    if (!isset($_GET['_id'])) {
        $html .= '<th></th>';
    }
    return $html;
}

function get_table_body($items)
{
    $html = '';

    foreach ($items as $row) {
        $html .= '<tr>';
        foreach ($row as $key => $value) {
            if ($key === 'completato') {
                if ($value) {
                    $html .= "<td><a class='link-success text-decoration-none font-weight-bold'"
                        . " alt='Clicca per settarlo ad Incompleto' title='Clicca per settarlo ad Incompleto'"
                        . "  href='/todolistmongo/includes/updateTodo.php?id=" . $row['_id'] . "&stato=0'>"
                        . "Completato</a></td>";
                } else {
                    $html .= "<td><a class='link-danger text-decoration-none font-weight-bold'"
                        . " alt='Clicca per Completare' title='Clicca per Completare'"
                        . "  href='/todolistmongo/includes/updateTodo.php?id=" . $row['_id'] . "&stato=1'>"
                        . "Non Completato</a></td>";
                }
            } elseif ($key === 'creazione' || $key === 'completamento') {
                if (!($key === 'completamento' && $value === null)) {
                    $date_time = $value->toDateTime();
                    //$date_time = explode(' ', $value);
                    //$data = implode('/', array_reverse(explode('-', $date_time[0])));
                    //$data = date("d/m/Y", intval($date_time->format('r')));
                    $html .= "<td>" . $date_time->format('d/m/Y') . "</td>";
                }
            } else {
                $html .= "<td>$value</td>";
            }
        }
        if (!isset($row['completamento'])) {
            $html .= "<td>&nbsp;</td>";
        }
        $html .= '<td>';
        $html .= '<a class="text-decoration-none" href="/todolistmongo/includes/delTodo.php?id='
            . $row['_id'] . '"><i class="fas fa-trash fa-lg" style="color: gray"></i></a>';
        $html .= '&nbsp;<a class="text-decoration-none link-light" href="/todolistmongo/index.php'
            . '?action=loadtodo&id=' . $row['_id'] . '"><i class="fas fa-pen fa-lg" style="color: darkorange"></i>'
            . '</a></td>';
        $html .= '</tr>';
    }

    return $html;
}

function show_alert($state, $message)
{
    if ($state === 'ko') {
        echo '<div class="alert alert-danger alert-dismissible" role="alert">' . $message
            . '<a href="./index.php" class="btn-close" style="cursor: pointer;"'
            . ' data-bs-dismiss="alert" aria-label="Close"></a></div>';
        return;
    }
    if ($state === 'ok') {
        echo '<div class="alert alert-success alert-dismissible" role="alert">' . $message
            . '<a href="./index.php" class="btn-close" style="cursor: pointer;"'
            . ' data-bs-dismiss="alert" aria-label="Close"></a></div>';
        return;
    }
}

trait InputSanitize
{
    public static function cleanInput($data)
    {
        $data = trim($data);
        $data = filter_var($data, FILTER_SANITIZE_ADD_SLASHES); // FILTER_SANITIZE_MAGIC_QUOTES
        $data = filter_var($data, FILTER_SANITIZE_SPECIAL_CHARS);
        return $data;
    }
}
