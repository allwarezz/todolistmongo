<?php
namespace DBHandle;

require_once __DIR__ . '/../vendor/autoload.php';

function getConnection()
{
    $client = new \MongoDB\Client(
        'mongodb+srv://daniele:2indRa9046MPyrMM@cluster0.e6o5g.mongodb.net/todo?retryWrites=true&w=majority'
    );
    $db = $client->todo;
    $listCollections = iterator_to_array($db->listCollectionNames());
    if (!in_array('tasks', $listCollections)) {
        $db->createCollection("tasks", [
            'validator' => [
                '$jsonSchema' => [
                    'bsonType' => "object",
                    'required' => ["task", "completato", 'creazione'],
                    'properties' => [
                        'task' => [
                            'bsonType' => "string",
                            'description' => 'deve essere una stringa ed è obbligatorio.',
                        ],
                        'completato' => [
                            'bsonType' => 'bool',
                            'description' => 'deve essere un boolean ed è obbligatorio',
                        ],
                        'creazione' => [
                            'bsonType' => 'date',
                            'description' => 'Deve essere una data ed è obbligatoria',
                        ],
                        'completamento' => [
                            'bsonType' => ['date', 'null'],
                            'description' => 'Non è Obbligatorio. Se presente deve essere una data o nullo',
                        ],
                    ],
                ],
            ],
        ]);
    }
    return $db;
}
